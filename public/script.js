'use strict';
const btnHamburger = document.querySelector('.button-hamburger');
const headerBottom = document.querySelector('.header-wrapper-bottom');
const loginSection = document.querySelector('.login-section-wrapper');
const loginButton = document.querySelector('.button-login');
const closeButton = document.querySelector('.button-close');
// const sliderWrapper = document.querySelector('.slider-wrapper');


// Access Login Button
const toggleLogin = function () {
  // const top = sliderWrapper.getBoundingClientRect().top;
  // loginSection.style.top = `${-top}`;
  loginSection.classList.toggle('hidden');
  btnHamburger.classList.remove('clicked');
  headerBottom.classList.add('hidden');
};
loginButton.onclick = closeButton.onclick = toggleLogin;

// Hamburger Menu
const toggleMenu = function () {
  btnHamburger.classList.toggle('clicked');
  headerBottom.classList.toggle('hidden');
};
btnHamburger.onclick = toggleMenu;

// Slider Component
const slider = function () {
  const slidesCont = document.querySelector('.slides');
  const slides = document.querySelectorAll('.slide');
  const btnPrev = document.querySelector('.btn-prev');
  const btnNext = document.querySelector('.btn-next');
  const dotsContainer = document.querySelector('.slider-dots');
  const dots = document.querySelectorAll('.slider-dot');

  let curSlide = 0;
  const maxSlide = slides.length - 1;
  const xCoords = ["0", "-100vw", "-200vw"]
  // Methods

  const activateDot = function (slide) {
    dots.forEach(dot => dot.classList.remove('active'));
    document.querySelector(`.slider-dot[data-slide="${slide}"]`).classList.add('active');
  };

  const goToSlide = function (slide) {
    slidesCont.style.transform = `translate3d(${xCoords[slide]}, 0, 0)`;
    slides.forEach(s => s.classList.remove('active'));
    document.querySelector(`.slide[data-slide="${slide}"]`).classList.add('active');
  };

  const nextSlide = function () {
    if (curSlide === maxSlide) {
      curSlide = 0;
    } else {
      curSlide++;
    }
    goToSlide(curSlide);
    activateDot(curSlide);
  };

  const prevSlide = function () {
    if (curSlide === 0) {
      curSlide = maxSlide;
    } else {
      curSlide--;
    }
    goToSlide(curSlide);
    activateDot(curSlide);
  };

  btnPrev.addEventListener('click', prevSlide);
  btnNext.addEventListener('click', nextSlide);

  dotsContainer.addEventListener('click', function (e) {
    if (e.target.classList.contains('slider-dot')) {
      const { slide } = e.target.dataset;
      goToSlide(slide);
      activateDot(slide);
    }
  });
};
slider();