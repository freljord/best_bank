import React from 'react';
import ReactDOM from 'react-dom';
import { Podcast } from './components/Podcast';
import { Blog } from './components/Blog';
import { Accomplish } from './components/Accomplish';
import { Plan } from './components/Plan';
import { Digital } from './components/Digital';
import { DigitalHero } from './components/DigitalHero';
import { Prensa } from './components/Prensa';
import { Communication } from './components/Communication';

const App = () => {
  return (
    <div >
      <Podcast />
      <Blog />
      <Accomplish />
      <Plan />
      <Digital />
      <DigitalHero />
      <Prensa />
      <Communication />
    </div>
  )
};

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);

