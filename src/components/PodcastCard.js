import React from 'react'
import '../css/talks.css';

export const PodcastCard = (props) => {
  return (
    <div className='podcast-card'>
      <div className='podcast-sub-card'>
        <a href='#'>
          <div className='podcast-image' style={{ backgroundImage: `url(${props.image})` }}>
            <div className='podcast-time'>
              <span>{props.time}</span>
            </div>
          </div>
        </a>
        <div className='podcast-sub-card-column'>
          <div className='podcast-sub-card-text'>
            <a href='#'>
              <p>{props.title}</p>
            </a>
            <p>{props.author}</p>
          </div>
          <div className='podcast-sub-card-button'>
            <div className='share-icon'>
              <i className="share alternate icon"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
