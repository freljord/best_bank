import React from 'react'
import '../css/prensa.css'
import { PrensaCard } from './PrensaCard'
import prensaImg1 from '../images/prensa-img-1.jpg'
import prensaImg2 from '../images/prensa-img-2.jpg'


export const Prensa = () => {
  return (
    <div className='prensa-container'>
      <div className='prensa-title'>
        <a href='#'>
          <h2>News</h2>
        </a>
      </div>
      <div className='prensa-cards-wrapper'>
        <PrensaCard title='Best Bank 1st place in World Bank Competition' image={prensaImg1} />
        <PrensaCard title='Best Bank obtains certification in bank secutiry' image={prensaImg2} />
      </div>
      <div className='prensa-link'>
        <a href='#'>
          <i className="angle right icon"></i>More news
        </a>
      </div>
    </div>
  )
}
