import React from 'react';
import '../css/blog.css';
import blogIcon from '../images/Blog-icon.svg';
import { BlogCard } from './BlogCard';
import blogImg1 from '../images/blog-img-1.jpg';
import blogImg2 from '../images/blog-img-2.jpg';


export const Blog = () => {
  return (
    <section className='blog'>
      <div className='blog-container'>
        <div className='blog-row'>
          <a href='#'>
            <img src={blogIcon}></img>
            <h3>Best Bank's Blog</h3>
          </a>
          <div className='blog-cards'>
            <BlogCard image={blogImg1} topic='NATURE' title='How to look out for nature and the environment' text='There is a presentation of ideas, with a mixure of arguments viewed from different perspectives on how to care for our planet.' />
            <BlogCard image={blogImg2} topic='SOCIAL' title='Fast pace of life today' text='Adaptation is a fundamental skill for the survival of any creature on this planet. A discussion on how to adapt to a fast paced world.' />
          </div>
        </div>
      </div>
    </section>
  );
};
