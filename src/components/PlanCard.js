import React from 'react'
import '../css/plan.css';

export const PlanCard = () => {
  return (
    <div className='plan-card'>
      <div className='card-product'>
        <span><i className='money bill alternate icon'></i></span>
        <h3>
          BEST
          <br></br>
          LOANS
        </h3>
        <p>Let us help you pay your debts.</p>
        <a href='#' className='product-link'>
          <i className='angle right icon'></i>
          Keep on reading
        </a>
      </div>
      <div className='card-product'>
        <span><i className='file outline icon'></i></span>
        <h3>
          SAVINGS
        </h3>
        <p>Lorem ipsum dolor sit amet.</p>
        <a href='#' className='product-link'>
          <i className='angle right icon'></i>
          Keep on reading
        </a>
      </div>
      <div className='card-product'>
        <span><i className='umbrella icon'></i></span>
        <h3>
          SECURE
          <br></br>
          INVESTMENTS
        </h3>
        <p>Best Insurances.</p>
      </div>
      <div className='card-product bottom'>
        <h3>
          <a href='#'>Student Loans</a>
        </h3>
        <p>Pay for your studies at the best rate.</p>
      </div>
      <div className='card-product bottom'>
        <h3>
          <a href='#'>Auto Loans</a>
        </h3>
        <p>Let us help you get the BEST car.</p>
      </div>
      <div className='card-product bottom'>
        <h3>
          <a href='#'>Entrepreneurship</a>
        </h3>
        <p>Set up your business, let us take the risk.</p>
      </div>
    </div>
  )
}
