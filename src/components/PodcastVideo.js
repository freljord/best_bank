import React from 'react';
import '../css/talks.css';

export const PodcastVideo = (props) => {
  return (
    <div className='video-card'>
      <div className='video-card-fade'>
        <div className='video-card-img' style={{ backgroundImage: `url(${props.image})` }}>
          <div className='video-card-row'>
            <div className='video-card-time'>
              <span>{props.time}</span>
            </div>
            <div className='video-card-column-icon'>
              <div>
                <a href='#'>
                  <i class="play circle outline icon"></i>
                </a>
              </div>
            </div>
          </div>
          <div className='video-card-row'>
            <div className='video-card-column'>
              <div className='video-card-text'>
                <a href='#'>
                  <h3>{props.title}</h3>
                </a>
                <p>{props.author}</p>
              </div>
              <div className='video-card-button'>
                <div className='video-share-icon'>
                  <i className="share alternate icon"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

