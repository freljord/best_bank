import React from 'react';
import '../css/digital.css';

export const Digital = () => {
  return (
    <div className='digital-wrapper'>
      <div className='digital-container'>
        <div className='digital-row'>
          <div className='digital-headline'>
            <h2>Ease of access to internet banking</h2>
            <a href='#'>
              <i className="angle right icon"></i>Watch how to
            </a>
          </div>
          <div className='digital-items-container'>
            <div className='digital-items-row'>
              <a href='#' className='desktop-icon'>
                <i className="desktop icon"></i>
                <span>
                  INTERNET BANKING
                </span>
              </a>
              <a href='#'>
                <i className="mobile alternate icon"></i>
                <span>
                  APP 1
                </span>
              </a>
              <a href='#'>
                <i className="mobile icon"></i>
                <span>
                  APP 2
                </span>
              </a>
            </div>
            <div className='digital-items-row'>
              <a href='#'>
                <i className="phone icon"></i>
                <span>
                  TELEAPP
                </span>
              </a>
              <a href='#'>
                <i className="map marker icon"></i>
                <span>
                  BANKMAP
                </span>
              </a>
              <a href='#'>
                <i className="map marker alternate icon"></i>
                <span>
                  CLOSESTBANK
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
