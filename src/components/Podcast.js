import React from 'react'
import '../css/talks.css';
import talkIcon from '../images/talk-icon.svg';
import { PodcastCard } from './PodcastCard';
import { PodcastVideo } from './PodcastVideo';
import podcastImg1 from '../images/podcast-img-1.png'
import podcastImg2 from '../images/podcast-img-2.png'
import videoImage1 from '../images/video-img-1.jpg';
import videoImage2 from '../images/video-img-2.jpg';



export const Podcast = () => {
  return (
    <div className='podcast-wrapper'>
      <div className='podcast'>
        <div className='podcast-container'>
          <div className='podcast-row'>
            <img src={talkIcon} alt='Talk Icon'></img>
            <h3>Talks</h3>
          </div>
          <div className='podcast-row'>
            <div className='podcast-column'>
              <a href='#'>
                <h4>Listen</h4>
              </a>
              <div className='podcast-cards'>
                <PodcastCard title='Virtue' image={podcastImg1} author='zhadox' time='10:07' />
                <PodcastCard title='Concentration' author='TiTo' image={podcastImg2} time='07:00' />
              </div>
            </div>
            <div className='podcast-column'>
              <div className='videos-wrapper'>
                <section className='videos'>
                  <div className='videos-row'>
                    <div className='videos-column'>
                      <a href='#'>
                        <h4>Videos</h4>
                      </a>
                    </div>
                  </div>
                  <section className='video-play'>
                    <div className='video-play-row'>
                      <div className='video-cards'>
                        <PodcastVideo title='How to improve performance' author='John Doe' image={videoImage1} time='4:20' />
                        <PodcastVideo title='Best practices of spending' author='Fudoo Mio' image={videoImage2} time='5:37' />
                      </div>
                    </div>
                    <a href='#'>
                      <i class="angle right icon"></i>Related content
                    </a>
                  </section>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
