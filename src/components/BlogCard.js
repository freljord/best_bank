import React from 'react';
import '../css/blog.css';

export const BlogCard = (props) => {
  return (
    <div className='blog-card'>
      <div className='blog-card-img' style={{ backgroundImage: `url(${props.image})` }}>
      </div>
      <div className='blog-info'>
        <div className='blog-info-top'>
          <span>{props.topic}</span>
          <div className='share-icon'>
            <i className="share alternate icon"></i>
          </div>
        </div>
        <div className='blog-info-middle'>
          <a href='#'>
            <p>{props.title}</p>
          </a>
          <p>{props.text}</p>
        </div>
        <div className='blog-info-bottom'>
          <a href='#'>More about this</a>
        </div>
      </div>
    </div >
  );
};
