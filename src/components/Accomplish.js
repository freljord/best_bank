import React from 'react'
import '../css/accomplish.css';
import { AccomplishCard } from './AccomplishCard';

export const Accomplish = () => {
  return (
    <section className='accomplish-wrapper'>
      <div className='accomplish-section'>
        <div className='accomplish-info'>
          <h2>Accomplish your
            <br></br>
            Dreams
          </h2>
          <p>
            We are here to offer you the best quality service to help you achieve your goals and live your dreams.
          </p>
          <div className='accomplish-links'>
            <a href='#'><i className='angle right icon'></i> Save</a>
            <a href='#'><i className='angle right icon'></i> Lorem ipsum</a>
            <a href='#'><i className='angle right icon'></i> Iste natus</a>
          </div>
        </div>
        <AccomplishCard />
      </div>
    </section>
  )
}
