import React from 'react'
import '../css/plan.css';
import { PlanCard } from './PlanCard';

export const Plan = () => {
  return (
    <section className='plan-wrapper'>
      <div className='plan-section'>
        <div className='plan-info'>
          <h2>Plan and
            <br></br>
            make it!
          </h2>
          <p>Lorem ipsum dolor sit amet.</p>
          <div className='plan-links'>
            <a href='#' className='plan-link'><i className='angle right icon'></i>Buy at best</a>
            <a href='#' className='plan-link'><i className='angle right icon'></i>Buy the best</a>
            <a href='#' className='plan-link'><i className='angle right icon'></i>Buy with best</a>
            <a href='#' className='plan-link'><i className='angle right icon'></i>Increase return</a>
          </div>
        </div>
        <PlanCard />
      </div>
    </section>
  )
}
