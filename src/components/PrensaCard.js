import React from 'react'

export const PrensaCard = (props) => {
  return (
    <div className='prensa-card'>
      <div className='card-img' style={{ backgroundImage: `url(${props.image})` }}>

      </div>
      <div className='card-info'>
        <div className='info-top'>
          <span>PRESS RELEASE</span>
          <div className='share-icon'>
            <i className="share alternate icon"></i>
          </div>
        </div>
        <div className='info-middle'>
          <a href='#'>
            <p>{props.title}</p>
          </a>
        </div>
        <div className='info-bottom'>
          <a href='#'>
            Seguir leyendo
          </a>
        </div>
      </div>
    </div>
  )
}
