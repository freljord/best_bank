import React from 'react'
import '../css/communication.css'

export const Communication = () => {
  return (
    <section className='communication-wrapper'>
      <div className='communication-container'>
        <h2>Contact Us</h2>
        <div className='communication-row'>
          <div className='communication-item'>
            <a href='#'>
              <i className='envelope icon'></i>
              <h3>Write</h3>
            </a>
          </div>
          <div className='communication-item'>
            <a href='#'>
              <i className='comment alternate outline icon'></i>
              <h3>Chat with us</h3>
            </a>
          </div>
          <div className='communication-item'>
            <a href='#'>
              <i className='phone icon'></i>
              <h3>Call</h3>
            </a>
          </div>
          <div className='communication-item'>
            <a href='#'>
              <i className='calendar alternate ouline icon'></i>
              <h3>Make an appointment</h3>
            </a>
          </div>
        </div>
      </div>
    </section>
  )
}
