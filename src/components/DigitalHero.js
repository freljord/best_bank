import React from 'react';
import '../css/digitalHero.css';

export const DigitalHero = () => {
  return (
    <div className='hero-img'>
      <div className='hero-img-fade'>
        <div className='hero-text-container'>
          <div className='hero-text-row'>
            <div className='hero-text-column'>
              <div className='hero-text-block'>
                <h2>INTERNET BANKING</h2>
                <p>
                  Get easy access to all the apps that you need.
                </p>
                <a href='#'>
                  More
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
