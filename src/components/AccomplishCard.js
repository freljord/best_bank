import React from 'react'
import '../css/accomplish.css';
import img1 from '../images/accomplish-img-1.jpg'
import img2 from '../images/accomplish-img-2.jpg'
import img3 from '../images/accomplish-img-3.jpg'



export const AccomplishCard = () => {
  return (
    <div className='accomplish-card'>
      <div className='card-product'>
        <span>
          <i className='credit card outline icon'></i>
        </span>
        <h3>CARDS</h3>
        <p>Get your card.</p>
        <a className='card-link'><i className='angle right icon'></i>Keep on reading</a>
      </div>
      <div className='card-product'>
        <span>
          <i className='address book outline icon'></i>
        </span>
        <h3>ACCOUNTS</h3>
        <p>Create and account.</p>
        <a className='card-link'><i className='angle right icon'></i>Keep on reading</a>
      </div>
      <div className='card-product'>
        <span>
          <i className='clipboard outline icon'></i>
        </span>
        <h3>PLAN</h3>
        <p>Set up a plan.</p>
        <a className='card-link'><i className='angle right icon'></i>Keep on reading</a>
      </div>
      <div className='card-product bottom'>
        <img src={img1}></img>
        <a><h3>BEST BANK'S CARD</h3></a>
        <p>ORDER your card right
          <a> here</a>
          .
        </p>
      </div>
      <div className='card-product bottom'>
        <img src={img2}></img>
        <a><h3>Best Account</h3></a>
        <p>Pay less get more, visit our
          <a> marketing strategies</a>: Lorem ipsum, dolor amet.
        </p>
        <h1>BankWebonline - Best Bank's Internet Service</h1>
      </div>
      <div className='card-product bottom'>
        <img src={img3}></img>
        <a><h3>UNIQUE ACCOUNT</h3></a>
        <p>Get BEST service at Best Bank.</p>
      </div>
    </div>
  )
}
